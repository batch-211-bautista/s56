function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    if (letter.length == 1) {
         // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
        let splitSentence = sentence.split("");

        for (let ctr = 0; ctr < splitSentence.length; ctr++) {
            if (letter.toString() == splitSentence[ctr].toString()) {
                result++;
            }
        }
        return result;

    } else if (letter.length > 1) {
        // If letter is invalid, return undefined.
        return undefined;
    }

}


function isIsogram(text) {

    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    return text.split("").every((element, index) => text.indexOf(element) == index) ? true : false;
    
}

function purchase(age, price) {

    // Return undefined for people aged below 13.
    if (age < 13) {
        return undefined;
    } 

    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    else if ((age >= 13 && age < 21) || (age > 64)) {
        let discountedPrice = (price - (price * 20 / 100)).toFixed(2);
        return String(discountedPrice);
    } 

    // Return the rounded off price for people aged 22 to 64.
    else if (age >= 22 && age <= 64) {
        let discountedPrice_2 = price.toFixed(2);
        return String(discountedPrice_2);
    }

    // The returned value should be a string.
    
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    const hotCategories = [];

    for (let ctr = 0; ctr < items.length; ctr++) {
        if(items[ctr].stocks == 0) {
            hotCategories.push(items[ctr].category);
        } else {
            continue;
        }
    }
    
    let result = hotCategories.filter((element, index, self) => {
        return self.indexOf(element) === index;
    });

    return result;
}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
    const flyingVoter = [];

    for (let ctr = 0; ctr < candidateA.length; ctr++) {
        for (let ctr1 = 0; ctr1 < candidateB.length; ctr1++) {
            if (candidateB[ctr1] == candidateA[ctr]) {
                flyingVoter.push(candidateB[ctr1]);
            } else {
                continue;
            }
        }
    }

    return flyingVoter;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};